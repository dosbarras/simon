¿Qué hay de nuevo?
==================

Composer
--------

Se agregó soporte para composer, ver instrucciones de instalación.

Propel
------

El plugin que traía integrado ha sido removido, debido a que no lo utilizamos
con frecuencia.