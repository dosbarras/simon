<?php

if (file_exists($composerAutoload = dirname(__FILE__).'/../vendor/autoload.php'))
{
  require_once($composerAutoload);
}
else
{
  require_once ##SYMFONY_CORE_AUTOLOAD##;
  sfCoreAutoload::register();  
}

class ProjectConfiguration extends sfProjectConfiguration
{
  public function setup()
  {
  }
}
