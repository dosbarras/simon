"simon" es un fork propio de symfony 1.4 para mantenerlo actualizado
y simple. El mismo reemplaza exactamente a instalaciones de 
symfony 1.4 ya que es retrocompatible.

En caso que se incorporen nuevas funcionalidades que rompen o 
requieren tareas adicionales para ser retrocompatible, las mismas
están indicadas en WHATS_NEW.md.

INSTALACIÓN
-----------

Vía composer, primero incluye en tu composer.json el repositorio
de dosbarras.com tal como dice en 
[http://composer.dosbarras.com](http://composer.dosbarras.com).

Luego puedes agregar el paquete a tus dependencias ejecutando:

    composer require dosbarras/simon:dev-master

Esto ha agregado simon a tus vendors. 

### Creando un nuevo proyecto

Ahora para crear tu proyecto sólo debes ejecutar:

    php vendor/bin/symfony generate:project nombre-proyecto autor

### Reemplazando symfony 1.4 en un proyecto existente

Sólo debes reemplazar las siguientes líneas de 
config/ProjectConfiguration.class.php:

    require_once dirname(__FILE__).'/../lib/vendor/symfony/lib/autoload/sfCoreAutoload.class.php';
    sfCoreAutoload::register();    

Por:

    require_once(dirname(__FILE__).'/../vendor/autoload.php');


DOCUMENTACIÓN OFICIAL
---------------------

Ver [http://symfony.com/legacy](http://symfony.com/legacy)
